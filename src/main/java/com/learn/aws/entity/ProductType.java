package com.learn.aws.entity;

public enum ProductType {
    LIQUID("litres"), COUNTABLE("items");

    private final String quantityStringAddition;

    ProductType(String quantityStringAddition) {
        this.quantityStringAddition = quantityStringAddition;
    }

    public String getQuantityStringAddition() {
        return quantityStringAddition;
    }
}