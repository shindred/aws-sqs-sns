package com.learn.aws.entity;

import java.util.Objects;

public class Order {
    private String productName;
    private ProductType productType;
    private int quantity;

    public Order() {
    }

    public Order(String productName, int quantity, ProductType productType) {
        this.productName = productName;
        this.quantity = quantity;
        this.productType = productType;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public ProductType getProductType() {
        return productType;
    }

    public void setProductType(ProductType productType) {
        this.productType = productType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Order order = (Order) o;
        return quantity == order.quantity &&
                Objects.equals(productName, order.productName) &&
                productType == order.productType;
    }

    @Override
    public int hashCode() {
        return Objects.hash(productName, productType, quantity);
    }

    @Override
    public String toString() {
        return "Order{" +
                "productName='" + productName + '\'' +
                ", productType=" + productType +
                ", quantity=" + quantity + " " + productType.getQuantityStringAddition() +
                '}';
    }
}
