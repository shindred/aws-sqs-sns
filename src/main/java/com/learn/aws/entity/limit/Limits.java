package com.learn.aws.entity.limit;

public enum Limits {
    LIQUIDS_LIMIT(50), COUNTABLE_LIMIT(50);

    private final int limit;

    Limits(int limit){
        this.limit = limit;
    }

    public int getLimit() {
        return limit;
    }
}
