package com.learn.aws.service;

import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.model.Message;
import com.learn.aws.s3.BucketLogger;
import com.learn.aws.sqs.MessageReceiver;

import java.util.List;

public class BucketLoggerService {

    private final BucketLogger bucketLogger;
    private final MessageReceiver messageReceiver;

    public BucketLoggerService() {
        this.bucketLogger = new BucketLogger();
        this.messageReceiver = new MessageReceiver();
    }

    public void logMessagesToBucketFile(AmazonSQS sqs, String queueUrl) {
        List<Message> messages = messageReceiver.getMessagesFromQueue(sqs, queueUrl);
        messages.forEach(m -> bucketLogger.logToBucketFile(m.getBody()));
    }
}
