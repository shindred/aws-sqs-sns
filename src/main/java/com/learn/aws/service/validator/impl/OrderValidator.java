package com.learn.aws.service.validator.impl;

import com.learn.aws.entity.Order;
import com.learn.aws.entity.limit.Limits;
import com.learn.aws.service.validator.Validator;

public class OrderValidator implements Validator<Order> {

    @Override
    public boolean validate(Order order) {
        return order.getQuantity() < Limits.COUNTABLE_LIMIT.getLimit() ||
                order.getQuantity() < Limits.LIQUIDS_LIMIT.getLimit();
    }
}
