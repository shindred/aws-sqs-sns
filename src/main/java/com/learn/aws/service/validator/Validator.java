package com.learn.aws.service.validator;

public interface Validator<T> {

    boolean validate(T target);

}
