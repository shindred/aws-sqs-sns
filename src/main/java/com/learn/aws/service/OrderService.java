package com.learn.aws.service;

import com.amazonaws.services.sqs.AmazonSQS;
import com.google.gson.Gson;
import com.learn.aws.entity.Order;
import com.learn.aws.service.validator.Validator;
import com.learn.aws.service.validator.impl.OrderValidator;
import com.learn.aws.sqs.MessageReceiver;
import com.learn.aws.sqs.MessageSender;

import java.util.List;
import java.util.stream.Collectors;

public class OrderService {

    private final MessageSender messageSender;
    private final Validator<Order> validator;
    private final MessageReceiver messageReceiver;

    public OrderService() {
        messageSender = new MessageSender();
        validator = new OrderValidator();
        messageReceiver = new MessageReceiver();
    }

    public void createOrder(AmazonSQS sqs, String queueUrl, Order order) {
        if (sqs == null || order == null) {
            throw new IllegalArgumentException();
        }

        String orderJson = new Gson().toJson(order);

        messageSender.sendMessage(sqs, queueUrl, orderJson);
    }

    public void getOrdersFromQueue(AmazonSQS sqs, String queueUrl) {
        List<Order> orderList = messageReceiver.getOrdersFromQueue(sqs, queueUrl);

        List<String> messages = orderList.stream()
                .map(o -> getMessage(o) + System.lineSeparator())
                .collect(Collectors.toList());

        messages.forEach(m -> messageSender.sendMessage(sqs, queueUrl, m));
    }

    private String getMessage(Order order) {
        StringBuilder messageBuilder = new StringBuilder();

        if (validator.validate(order)) {
            messageBuilder.append("Order created!")
                    .append(System.lineSeparator())
                    .append(order);
        } else {
            messageBuilder.append("Order not created due to exceeding the limit!")
                    .append(System.lineSeparator())
                    .append("The order was: ")
                    .append(System.lineSeparator())
                    .append(order);
        }

        return messageBuilder.toString();
    }
}
