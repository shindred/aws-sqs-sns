package com.learn.aws;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSClientBuilder;
import com.learn.aws.entity.Order;
import com.learn.aws.entity.ProductType;
import com.learn.aws.service.BucketLoggerService;
import com.learn.aws.service.OrderService;

public class Main {

    private static final String QUEUE_NAME = "MenteeQueue";
    private static final AmazonSQS SQS = AmazonSQSClientBuilder.standard().withRegion(Regions.US_EAST_1).build();

    public static void main(String[] args) {
        String queueUrl = SQS.getQueueUrl(QUEUE_NAME).getQueueUrl();

        BucketLoggerService bucketLoggerService = new BucketLoggerService();
        OrderService orderService = new OrderService();

        orderService.createOrder(SQS, queueUrl, new Order("defaultProduct", 186, ProductType.COUNTABLE));

        orderService.getOrdersFromQueue(SQS, queueUrl);

        bucketLoggerService.logMessagesToBucketFile(SQS, queueUrl);
    }
}
