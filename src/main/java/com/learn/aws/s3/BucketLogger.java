package com.learn.aws.s3;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.S3Object;
import org.apache.commons.io.FileUtils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class BucketLogger {

    private static final String BUCKET_NAME = "mentee-bucket";
    private static final String LOG_FILE_NAME = "order-log.txt";
    private static final AmazonS3 S3 = AmazonS3ClientBuilder.standard().withRegion(Regions.US_EAST_1).build();

    public void logToBucketFile(String message) {
        File file = new File(LOG_FILE_NAME);
        S3Object bucket = S3.getObject(BUCKET_NAME, LOG_FILE_NAME);

        try (BufferedWriter writer = new BufferedWriter(new FileWriter(file, true))) {
            if (bucket != null) {
                FileUtils.copyInputStreamToFile(bucket.getObjectContent(), file);
            }

            writer.write(System.lineSeparator());
            writer.write(System.lineSeparator());
            writer.write(message);

        } catch (IOException e) {
            e.printStackTrace();
        }

        S3.putObject(BUCKET_NAME, LOG_FILE_NAME, file);
    }
}
