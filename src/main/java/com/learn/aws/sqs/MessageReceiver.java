package com.learn.aws.sqs;

import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.model.Message;
import com.google.gson.Gson;
import com.learn.aws.entity.Order;

import java.util.List;
import java.util.stream.Collectors;

public class MessageReceiver {
    public List<Order> getOrdersFromQueue(AmazonSQS sqs, String queueUrl) {
        List<Message> messages = sqs.receiveMessage(queueUrl).getMessages();

        List<Order> orders =  messages.stream()
                .map(m -> new Gson().fromJson(m.getBody(), Order.class))
                .collect(Collectors.toList());

        messages.forEach(m -> sqs.deleteMessage(queueUrl, m.getReceiptHandle()));

        return orders;
    }

    public List<Message> getMessagesFromQueue(AmazonSQS sqs, String queueUrl){
        List<Message> messages = sqs.receiveMessage(queueUrl).getMessages();

        messages.forEach(m -> sqs.deleteMessage(queueUrl, m.getReceiptHandle()));

        return messages;
    }
}
