package com.learn.aws.sqs;

import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.model.SendMessageRequest;

public class MessageSender {

    public void sendMessage(AmazonSQS sqs, String queueUrl, String message) {
        sqs.sendMessage(new SendMessageRequest()
                .withQueueUrl(queueUrl)
                .withMessageBody(message));
    }
}
